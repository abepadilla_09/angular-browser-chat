  // define our app and dependencies (remember to include firebase!)
  var app = angular.module("chatApp", ["ngRoute", "firebase"]);

  // this factory returns a synchronized array of chat messages
  app.factory("chatMessages", ["$firebaseArray",
    function($firebaseArray) {
      // create a reference to the Firebase database where we will store our data
      var randomRoomId = Math.round(Math.random() * 100000000);
      var ref = new Firebase("https://angular-chatapp.firebaseio.com/chat/" + randomRoomId);

      // this uses AngularFire to create the synchronized array
      return $firebaseArray(ref);
    }
  ]);


  //route
  app.config(function($routeProvider, $locationProvider){
    $routeProvider
      .when('/', {
        templateUrl : 'chat-login.html',
        controller : 'LoginController'
      })
      .when('/chat/:room', {
        templateUrl : 'chatbox.html',
        controller : 'ChatController'
      })
      .otherwise({
        redirectTo: '/'
      });

      $locationProvider.html5Mode(false);
  });

  //controllers
  app.controller('MainController', function($scope, $location, $route){
      $route.reload();
      $location.path("/");
  });

  app.controller('LoginController', function($scope){

  });

  app.controller("ChatController", ["$scope", "chatMessages",
    // we pass our new chatMessages factory into the controller
    function($scope, chatMessages) {
      $scope.user = "Guest " + Math.round(Math.random() * 100);

      // we add chatMessages array to the scope to be used in our ng-repeat
      $scope.messages = chatMessages;

      // a method to create new messages; called by ng-submit
      $scope.addMessage = function() {
        // calling $add on a synchronized array is like Array.push(),
        // except that it saves the changes to our Firebase database!
        $scope.messages.$add({
          from: $scope.user,
          content: $scope.message
        });

        // reset the message input
        $scope.message = "";
      };



    }
  ]);